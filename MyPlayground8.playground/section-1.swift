// Playground - noun: a place where people can play

// データ型の拡張について
// extensionで機能拡張してみよう
// StringとかIntの基本データ型以外にも、クラスとか構造体とか列挙型にも使える

// String型に新しいメソッドや新しいプロパティを追加
extension String {
    var size: Int {
        return countElements(self)
    }
    func dummy() -> String {
        return "dummy"
    }
}

var s: String = "hoge222"
s.size
s.dummy()


