// Playground - noun: a place where people can play

//import UIKit
//
//var str = "Hello, playground"

// optional chaining

class User {
    var blog: Blog?
}

class Blog {
    var title = "My BLog"
}

var tom = User()
tom.blog = Blog()
// tom.blog?.title

if let t = tom.blog?.title {
    println(t)
}
