// Playground - noun: a place where people can play

//import UIKit
//
//var str = "Hello, playground"

/*
クラス (User)　→ インスタンス(tom, bob)
- 変数 name, score: プロパティ
- 関数 sayHi(): メソッド

クラスの継承
*/

class User {
    var name: String
//    var name: String?
    var score: Int = 0
    init(name: String) {
        self.name = name
    }
    
//    final func upgrade() {
//        score++
//    }
    
    func upgrade() {
        score++
    }
}

class AdminUser: User {
    func reset() {
        score = 0
    }
    
    override func upgrade() {
        super.upgrade()
        score += 3
    }
}

var tom = User(name: "Tom")
tom.name
tom.score
tom.upgrade()
tom.score

var bob = AdminUser(name: "Bob")
bob.name
bob.score
bob.upgrade()
bob.score
bob.reset()
bob.score
//tom.reset()

 

