// Playground - noun: a place where people can play

// 構造体
// クラスとほぼ似たような感じでプロパティとかメソッドをまとめたデータ型になります。
// さらにプロトコルとかイニシャライザーとかも使うことができます。

// 構造体は継承はできない
struct UserStruct {
    var name: String
    var score: Int = 0
    init(name: String) {
        self.name = name
    }
    // 構造体ではメソッドの中で自身のプロパティを書き換えることができない
    // 書き換える場合は明示的に指示をする必要があって、
    // funcの前にmutatingというキーワードを付ける必要がある
    mutating func upgrade() {
        score++
    }
}


class User {
    var name: String
    var score: Int = 0
    init(name: String) {
        self.name = name
    }
    func upgrade() {
        score++
    }
}

// 3つ目の違いはコピーするときの挙動がちょっと違います。
// 構造体は値渡し、クラスは参照渡しになっています。

var tom = User(name: "Tom")
// 参照コピー
var tom2 = tom
tom2.name = "tom2"
tom.name
// メモリを効率的に使うために

var bob = UserStruct(name: "Bob")
// 構造体は値渡し
var bob2 = bob
bob2.name = "bob2"
bob.name
// 構造体は参照を渡しているのではなくて、値をそのまま丸ごとコピーして、
// そのまま別のデータを作ってくれるからこのようになっています。
// 大きな構造体だと、コピーするたびにメモリを圧迫するので、注意が必要

// クラスと構造体の使い分けは
// 継承が必要だったり、大きなデータを扱う場合にはクラス、
// シンプルなデータを扱う場合には、構造体という具合に使い分ければいいと思います。


