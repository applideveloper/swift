// Playground - noun: a place where people can play


// type casting
// クラスの型をチェックしたり、
// あるクラスを親くらすとか子クラスとして扱うための方法となります。

class User {
    var name: String
    init(name: String) {
        self.name = name
    }
}

class AdminUser: User {}
class SomeUser {}

let tom = User(name: "Tom")
let bob = AdminUser(name: "Bob")
let steve = SomeUser()

let users: [AnyObject] = [tom, bob, steve];
//let users = [tom, bob]
//let uers: [User] = [tom, bob]

for user in users {
//    if user is AdminUser {
//        let u = user as AdminUser
//        println(u.name)
//    }
    if let u = user as? AdminUser {
        println(u.name)
    }
}

