// コメント
/*
    コメント
    コメント
*/
print("hello world")
println("hello world")

// 変数: データにつけるラベル var
// 定数: 変更ができない変数 let
//var msg: String
//msg = "hello world"

var msg = "hello world"
// print(msg)

let s = "hoge"
// s = "bar"

println(s)

println("msg: \(msg), s: \(s)")

// 基本データ型
/*
String          "hoge"
Int             36
Float/Double    5.234234234
Bool            true false
nil
*/

// + - * / %
let x: Float = 8.0 % 2.5
x

// ++ --
var y = 0
y++
y

++y
y

// +
let t = "hello " + "world"

// && || !
true && false
true || false
!true

// conversion
let a = "hkt"
let b = 48
let c = a + String(b)

// タプル

// let error = (404, "not found")
// error.0
// error.1

//let error = (code:404, msg:"not found")
//error.code
//error.msg

let error = (404, "not found")
// let (code, message) = error
// _ :値を安全に破棄
let (code, _) = error
code
// message

// 配列
var colors: [String] = ["blue", "pink"];
colors[0] // 添え字
colors[1] = "red"
colors

colors.count
colors.isEmpty

colors.append("green")
colors.insert("gray" , atIndex: 1)
colors

colors.removeAtIndex(1)
colors

let secondColor = colors.removeAtIndex(1)
secondColor

colors.removeLast()
colors

var emptyArray = [String]()

// 辞書
// key: value
var users: [String: Int] = [
    "taguchi": 500,
    "fkoji": 800
]

users["taguchi"]

users.count
users.isEmpty

users["dotinstall"] = 900
users

users.removeValueForKey("dotinstall")
users

// users["fkoji"] = 900
users.updateValue(900, forKey: "fkoji")
users

let keys = Array(users.keys)
let values = Array(users.values)

var emptyDictionary = [String: Int]()
emptyDictionary

// if
// > >= < <= == !=
// && || !
let score = 62
var result = ""

if score > 80 {
    result = "score > 80"
} else if score > 60 {
    result = "60 < score <= 80"
} else {
    result = "score <= 60"
}

result = score > 80 ? "score > 80" : "score <= 80"

// switch
let num = 0

switch num {
case 0:
    println("zero")
    fallthrough
case 1, 2, 3:
    println("small")
case 4...6: // 4, 5, 6
    println("4/5/6")
case 7..<9: // 7, 8
    println("7/8")
case let n where n > 10:
    println("hoge")
default:
    // println("n.a.")
    break;
}

// while
var n = 20
//while n < 10 {
//    println(n)
//    n++
//}

do {
    println(n)
    n++
} while n < 10

// for
//for var i = 0; i < 10; i++ {
//    if i == 3 {
//        // continue
//        break
//    }
//    println(i)
//}

// 範囲演算子
//for i in 0...9 {
//    println(i)
//}

//let d: [Int] = [5, 3, 19]
let d = [5, 3, 19]

for i in d {
    println(i)
}

let e = ["taguchi": 500, "fkoji": 800]
for (k, v) in e  {
    println("key:\(k) val:\(v)")
}

// Optional
//var u: String?
//u = nil

let name: String? = "taguchi"
//let message2 = "hello " + name!

//if name != nil {
//    let message2 = "hello " + name!
//    println(message2)
//}

//if let v = name {
//    let message3 = "hello " + v
//    println(message3)
//}

var label: String!
label = "score"
println(label)

// 関数1
//func sayHi() {
//    println("Hi")
//}
//sayHi()

// 引数
//func sayHi(myname name: String) {
//    println("Hi " + name)
//}
//sayHi(myname: "Tom");

//func sayHi(#name: String) {
//    println("Hi " + name)
//}
//sayHi(name: "Tom");

func sayHi(#name: String = "Tom") {
    println("Hi " + name)
}
sayHi()
sayHi(name: "Bob");

// 関数2
//func sum(a:Int, b:Int) -> Int {
//    return a + b
//}
//println(sum(5, 12))

//func swap(a:Int, b:Int) -> (Int, Int) {
//    return (b, a)
//}
//println(swap(5, 2))

// 値のコピーなのでg変わらない
//func f(var g:Int) {
//    g = 20
//}
//var g = 5
//f(g)
//g

func f(inout g:Int) {
    g = 20
}
var g = 5
f(&g)
g

// 列挙型
//enum Result {
//    case Success
//    case Error
//}
enum Result: Int {
    case Success = 0
    case Error
    func getMessage() -> String {
        switch self {
        case .Success:
            return "Success!"
        case .Error:
            return "Error!"
        }
    }
}

//var r: Int 
var r: Result
// r = Result.Success
r = .Success

Result.Error.rawValue
Result.Error.getMessage()
