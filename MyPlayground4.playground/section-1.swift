// Playground - noun: a place where people can play

//import UIKit
//
//var str = "Hello, playground"

/*
クラス
get/set, willSet/didSetを使ってみよう
*/

class User {
    var name: String
    var score: Int = 0 {
        // プロパティが初期化されたときに最初に実行されない
        willSet {
            println("\(score) -> \(newValue)")
        }
        didSet {
            println("\(oldValue) -> \(score)")
        }
    }
    var level: Int {
        return Int(score / 10)
//        get {
////            return Int(self.score / 10)
//            return Int(score / 10)
//        }
//        set {
//            score = Int(newValue * 10)
//        }
    }
    
    init(name: String) {
        self.name = name
    }
    func upgrade() {
        score++
    }
}

var tom = User(name: "tom")
//tom.level
//tom.score = 52
//tom.level
//tom.level = 8
//tom.score
tom.score = 32

