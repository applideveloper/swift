// Playground - noun: a place where people can play

//import UIKit
//
//var str = "Hello, playground"

// プロトコル
protocol Student {
    var studentId: String { get set }
    func study()
}

class User: Student {
    var name: String
    var score: Int = 0
    var studentId: String = "hoge"
    
    func study() {
        println("studying...")
    }
    
    init(name: String) {
        self.name = name
    }
    func upgrade() {
        score++
    }
}

var tom = User(name: "Tom")

