// Playground - noun: a place where people can play

// ジェネリクス
// 抽象化されたデータ型

// 例えば、ある整数を指定した個数分だけ集めた配列を返す関数が欲しかった
//func getIntArray(item: Int, count: Int) -> [Int] {
//    var result = [Int]()
//    // iはつかってないので、安全に値を破棄してくれる「_」を使ってもいいかと思います。
//    for _ in 0..<count {
//        result.append(item)
//    }
//    return result
//}
//
//getIntArray(8, 3)

func getArray<T>(item: T, count: Int) -> [T] {
    var result = [T]()
    // iはつかってないので、安全に値を破棄してくれる「_」を使ってもいいかと思います。
    for _ in 0..<count {
        result.append(item)
    }
    return result
}

getArray(8, 3)
getArray("hello", 3)
getArray(2.3, 4)

 